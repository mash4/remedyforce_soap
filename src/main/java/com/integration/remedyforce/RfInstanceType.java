package com.integration.remedyforce;

public enum RfInstanceType {
	SANDBOX("https://test.salesforce.com/services/Soap/u/35.0")
	,PRODUCTION("https://login.salesforce.com/services/Soap/u/35.0");
	
	private String value;
	
	private RfInstanceType(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
	
}	
