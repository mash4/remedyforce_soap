package com.integration.remedyforce;

import java.io.IOException;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.automationedge.enums.WorkflowStatus;
import com.automationedge.integration.util.JsonUtils;
import com.automationedge.model.AutomationRequest;
import com.sforce.soap.VyomAutomation_TicketUpdate.AutomationResponse;
import com.sforce.soap.VyomAutomation_TicketUpdate.AutomationStatusResponse;
import com.sforce.soap.VyomAutomation_TicketUpdate.SoapConnection;
import com.sforce.soap.partner.Connector;
import com.sforce.soap.partner.PartnerConnection;
import com.sforce.ws.ConnectionException;
import com.sforce.ws.ConnectorConfig;

public class RfUtil {
	private static final Logger LOGGER = LoggerFactory.getLogger(RfUtil.class);
	
	private static final String SOURCE_REMEDYFORCE_NAMESPACE = "automationedge";
	private static final String SOURCE_REMEDYFORCE = "Remedyforce";
	private static final String VYOM_AUTOMATION_TICKET_UPDATE = "/VyomAutomation_TicketUpdate";
	
	private RfConfig rfConfig = null;
	
	private static RfUtil instance = null;
	
	public static RfUtil getInstance() {
		if(instance == null) {
			instance = new RfUtil();
		}
		return instance;
	}
	
	public List<AutomationRequest> getAutomationRequests(Object...args) {
		buildRfConfigObject(args);
		
		SoapConnection soapConn = getSForceAutomationSoapConnection();
		if(soapConn == null) {
			LOGGER.debug("SOAP Connection object is null");
		}
		else {
			try {
				List<String> newReqs = Arrays.asList(soapConn.getNewRequests());
				if(newReqs != null && !newReqs.isEmpty()) {
					LOGGER.info("Received <{}> requests from source <{}>",newReqs.size(), SOURCE_REMEDYFORCE);
					if(newReqs != null && newReqs.isEmpty()) {
						LOGGER.debug("AE Request List is empty/null");
					}
					else {
						List<AutomationRequest> requests = new ArrayList<>();
						for (String requestString : newReqs) {
							try {
								requests.add(JsonUtils.deserialize(requestString, AutomationRequest.class));
							} catch (IOException e) {
								LOGGER.error("Source: "+ SOURCE_REMEDYFORCE + ", Could not parse request json: "+requestString, e);
							}
						}
						return requests;
					}
				}
			} catch (ConnectionException e) {
				LOGGER.error("Error connecting Remedyforce instance while fetching requests.", e);
			}
			
		}
		return Collections.emptyList();
	}
	
	public void notifyAutomationRequestStatus(List<com.automationedge.integration.model.AutomationStatusResult> statusList, Object... args) {
		buildRfConfigObject(args);
		
		SoapConnection soapConn = getSForceAutomationSoapConnection();
		if(soapConn == null) {
			LOGGER.debug("SOAP Connection object is null");
			return;
		}
		
		if(statusList != null && !statusList.isEmpty()) {
			List<AutomationStatusResponse> statusRespList = new ArrayList<>();
			List<com.automationedge.model.AutomationResponse> errorRespList = new ArrayList<>();
			
			for (com.automationedge.integration.model.AutomationStatusResult asr: statusList) {
				LOGGER.debug(asr.toString());
				
				AutomationStatusResponse responseStatus = new AutomationStatusResponse();
				responseStatus.setSource(SOURCE_REMEDYFORCE);
				responseStatus.setSourceId(asr.getSourceId());
				
				if(WorkflowStatus.InProgress == asr.getStatus()) {
					responseStatus.setSuccess(true);
					responseStatus.setMessage(asr.getMessage());
					statusRespList.add(responseStatus);
				}
				else if(WorkflowStatus.Failure == asr.getStatus()){
					com.automationedge.model.AutomationResponse ar = new com.automationedge.model.AutomationResponse();
					ar.setSource(SOURCE_REMEDYFORCE);
					ar.setSourceId(asr.getSourceId());
					ar.setAutomationRequestId(Long.parseLong(asr.getAutomationRequestId()));
					ar.setSuccess(false);
					ar.setErrorDetails(asr.getMessage());
					
					errorRespList.add(ar);
				}
				else {
					LOGGER.warn("Unknown status found, ", asr.toString());
				}
				
			}
			
			if(!statusRespList.isEmpty()) {
				AutomationStatusResponse[] statusResponseArr = new AutomationStatusResponse[statusRespList.size()];
				statusResponseArr = statusRespList.toArray(statusResponseArr);
				
				try {
					soapConn.updateRequestStatus(statusResponseArr);
				} catch (ConnectionException e) {
					LOGGER.error("Error connecting Remedyforce instance while updating request status.", e);
				}
				LOGGER.info("Status updated successfully for <{}> requests", statusRespList.size());
			}
			
			if(!errorRespList.isEmpty()) {
				for (com.automationedge.model.AutomationResponse ar : errorRespList) {
					sendAutomationErrorResponse(soapConn, ar);
					LOGGER.error("Notified with the failure response, Source: <{}>, SourceId: <{}>", SOURCE_REMEDYFORCE, ar.getSourceId());
				}
			}
			
		}
		
	}
	
	private void sendAutomationErrorResponse(SoapConnection sfConnection, com.automationedge.model.AutomationResponse aeResponse) {
		LOGGER.info("Sending error response, Source: <{}>, SourceId: <{}>, UserId: <{}>, AutomationId: <{}>", 
				aeResponse.getSource(), aeResponse.getSourceId(), aeResponse.getUserId(), aeResponse.getAutomationRequestId());
		try{	
			String aeResponseJson = JsonUtils.serialize(aeResponse);
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("AE Response JSON String: {}", aeResponseJson);
			}
			
			AutomationResponse ar = new AutomationResponse(); 
			ar.setResponse(aeResponseJson);
			
			sfConnection.UpdateRequest(ar);
			LOGGER.info("Update successful for, Source: <{}>, SourceId: <{}>, UserId: <{}>, AutomationId: <{}>",
					aeResponse.getSource(), aeResponse.getSourceId(), aeResponse.getUserId(), aeResponse.getAutomationRequestId());
		}catch(Exception e){
			LOGGER.error("Update failed for, Source: <{}>, SourceId: <{}>, UserId: <{}>, AutomationId: <{}> \n{}",
					aeResponse.getSource(), aeResponse.getSourceId(), aeResponse.getUserId(), aeResponse.getAutomationRequestId(), e);
		}
	}
	
	private void setProxyParameters(ConnectorConfig config, final RfConfig rfConfig) {
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("Setting Proxy Details");
		}
		if(rfConfig.isKeyStore()) {
			System.setProperty("javax.net.ssl.trustStore", rfConfig.getKeyStorePath());
    		System.setProperty("javax.net.ssl.trustStorePassword", rfConfig.getKeyStorePassword());
    	}
		config.setProxy(rfConfig.getProxyHost(), rfConfig.getProxyPort());
    	if(rfConfig.isProxyCredentials()) {
    		Authenticator authenticator = new Authenticator() {
    	        public PasswordAuthentication getPasswordAuthentication() {
    	            return (new PasswordAuthentication(rfConfig.getProxyUser(),
    	            		rfConfig.getProxyPassword().toCharArray()));
    	        }
    	    };
    	    Authenticator.setDefault(authenticator);
    	}
	}
	
	private PartnerConnection getSForcePartnerConnection(RfConfig rfConfig) throws ConnectionException {
		ConnectorConfig config = new ConnectorConfig();
		config.setAuthEndpoint(RfInstanceType.SANDBOX.getValue());
        config.setUsername(rfConfig.getUserName());
        config.setPassword(rfConfig.getPassword());
        
        if(rfConfig.isProxy()) 
        	setProxyParameters(config, rfConfig);
        
		PartnerConnection connection = Connector.newConnection(config);
		return connection;
	}
	
	private RfConfig buildRfConfigObject(Object... args) {

		if(args.length > 0 && rfConfig == null) {
			String instType = String.valueOf(args[0]);
			String username = String.valueOf(args[1]);
			String pass = String.valueOf(args[2]);
			
			rfConfig = new RfConfig();
			rfConfig.setInstanceType(instType);
			rfConfig.setUserName(username);
			rfConfig.setPassword(pass);
		}
		
		return rfConfig;
	}
	
	private SoapConnection getSForceAutomationSoapConnection() {
		if(rfConfig != null) {
			if(RfInstanceType.SANDBOX.name().equalsIgnoreCase(rfConfig.getInstanceType())) {
				rfConfig.setLoginurl(RfInstanceType.SANDBOX.getValue());
			}
			else if(RfInstanceType.PRODUCTION.name().equalsIgnoreCase(rfConfig.getInstanceType())) {
				rfConfig.setLoginurl(RfInstanceType.PRODUCTION.getValue());
			}
			else {
				LOGGER.error("Invalid Instance Type: ", rfConfig.getInstanceType());
				return null;
			}
		}
		rfConfig.setLoginurl(RfInstanceType.SANDBOX.getValue());
		System.out.println(rfConfig.getLoginurl());
		System.out.println(rfConfig.getUserName());
		System.out.println(rfConfig.getPassword());
		PartnerConnection connection = null;
		SoapConnection sfConnection = null;
		try {
			connection = getSForcePartnerConnection(rfConfig);
			sfConnection = com.sforce.soap.VyomAutomation_TicketUpdate.Connector.newConnection("","");
			if(rfConfig.isProxy())
	        	setProxyParameters(sfConnection.getConfig(), rfConfig);
			
			String[] updateServiceEndPointStr = connection.getConfig().getServiceEndpoint().split("Soap/");
			String updateServiceEndPoint = updateServiceEndPointStr[0] + "Soap/class/VyomAutomation_TicketUpdate";
			if(rfConfig.getNamespace() != null && !rfConfig.getNamespace().isEmpty())
				updateServiceEndPoint = updateServiceEndPointStr[0] + "Soap/class/" + rfConfig.getNamespace() + VYOM_AUTOMATION_TICKET_UPDATE;
			else
				updateServiceEndPoint = updateServiceEndPointStr[0] + "Soap/class/" + SOURCE_REMEDYFORCE_NAMESPACE + VYOM_AUTOMATION_TICKET_UPDATE;
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("WS End-Point: " + updateServiceEndPoint);
			}
				
			String sessionId = connection.getSessionHeader().getSessionId();
			sfConnection.setSessionHeader(sessionId);
			sfConnection.getConfig().setServiceEndpoint(updateServiceEndPoint);
		} catch (ConnectionException e) {
			LOGGER.error("Error connecting Remedyforce instance", e);
		}
		
		return sfConnection;
	}
	
//	public List<String> getAutomationRequests(SoapConnection sfConnection) {
//		if(sfConnection != null) {
//			try {
//				List<String> newReqs = Arrays.asList(sfConnection.getNewRequests());
//				if(newReqs != null && !newReqs.isEmpty()) {
//					LOGGER.info("Received <{}> requests from source <{}>",newReqs.size(), SOURCE_REMEDYFORCE);
//					return newReqs;
//				}
//			} catch (ConnectionException e) {
//				LOGGER.error("Error connecting Remedyforce instance while fetching requests.", e);
//			}
//			
//		}
//		return Collections.emptyList();
//	}
	
	
}
