package com.integration.remedyforce;

public class AutomationStatusResult {
	
	private String sourceId;
	private String automationRequestId;
	private String status; 
	private String message;
	
	public String getSourceId() {
		return sourceId;
	}
	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}
	public String getAutomationRequestId() {
		return automationRequestId;
	}
	public void setAutomationRequestId(String automationRequestId) {
		this.automationRequestId = automationRequestId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AutomationStatusResult [sourceId=");
		builder.append(sourceId);
		builder.append(", automationRequestId=");
		builder.append(automationRequestId);
		builder.append(", status=");
		builder.append(status);
		builder.append(", message=");
		builder.append(message);
		builder.append("]");
		return builder.toString();
	}
	
	
}
