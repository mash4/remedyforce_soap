package com.integration.remedyforce;

public class RfConfig {
	private String loginurl;
	private String userName;
	private String password;
	/*
	 * Namespace of the Managed package which is going to be AutomationEdge
	 * If for whatever reasons, application gets deployed as unmanaged package, namespace would be null or ""
	 */
	private String namespace;
	private boolean proxy;
	private String proxyHost;
	private int proxyPort;
	private boolean proxyCredentials;
	private String proxyUser;
	private String proxyPassword;
	private boolean keyStore;
	private String keyStorePath;
	private String keyStorePassword;
	private String instanceType;
	public String getInstanceType() {
		return instanceType;
	}
	public void setInstanceType(String instanceType) {
		this.instanceType = instanceType;
	}
	public String getLoginurl() {
		return loginurl;
	}
	public void setLoginurl(String loginurl) {
		this.loginurl = loginurl;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getNamespace() {
		return namespace;
	}
	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}
	public boolean isProxy() {
		return proxy;
	}
	public void setProxy(boolean proxy) {
		this.proxy = proxy;
	}
	public String getProxyHost() {
		return proxyHost;
	}
	public void setProxyHost(String proxyHost) {
		this.proxyHost = proxyHost;
	}
	public int getProxyPort() {
		return proxyPort;
	}
	public void setProxyPort(int proxyPort) {
		this.proxyPort = proxyPort;
	}
	public boolean isProxyCredentials() {
		return proxyCredentials;
	}
	public void setProxyCredentials(boolean proxyCredentials) {
		this.proxyCredentials = proxyCredentials;
	}
	public String getProxyUser() {
		return proxyUser;
	}
	public void setProxyUser(String proxyUser) {
		this.proxyUser = proxyUser;
	}
	public String getProxyPassword() {
		return proxyPassword;
	}
	public void setProxyPassword(String proxyPassword) {
		this.proxyPassword = proxyPassword;
	}
	public boolean isKeyStore() {
		return keyStore;
	}
	public void setKeyStore(boolean keyStore) {
		this.keyStore = keyStore;
	}
	public String getKeyStorePath() {
		return keyStorePath;
	}
	public void setKeyStorePath(String keyStorePath) {
		this.keyStorePath = keyStorePath;
	}
	public String getKeyStorePassword() {
		return keyStorePassword;
	}
	public void setKeyStorePassword(String keyStorePassword) {
		this.keyStorePassword = keyStorePassword;
	}
}
