package com.integration.remedyforce;

import java.util.List;

import com.automationedge.integration.external.IAEIntegrationFramework;
import com.automationedge.integration.model.AutomationStatusResult;
import com.automationedge.model.AutomationRequest;
import com.automationedge.model.AutomationResponse;

public class MainController implements IAEIntegrationFramework
{
	public static void main(String[] ar) {
		Object[] args = new Object[3];
		args[0] = RfInstanceType.SANDBOX.name();
		args[1] = "saurabh.kulkarni@37bmcremedyforce.com.dev12";
		args[2] = "aedemo@123";
		
		MainController mc = new MainController();
		List<AutomationRequest> automationRequests = mc.getAutomationRequests(args);
		if(automationRequests != null && automationRequests.isEmpty()) {
			System.out.println("AE Request List is empty/null");
		}
	}

	@Override
	public List<AutomationRequest> getAutomationRequests(Object... args) {
		
		return RfUtil.getInstance().getAutomationRequests(args);
	}

	@Override
	public void notifyAutomationRequestStatus(List<AutomationStatusResult> statusList, Object... args) {
		RfUtil.getInstance().notifyAutomationRequestStatus(statusList, args);
	}

	@Override
	public void updateResponse(List<AutomationResponse> automationResponseList, Object... args) {
		// TODO Auto-generated method stub
		
	}
}
